module ksa-rp-client

go 1.17

require (
	github.com/dvsekhvalnov/jose2go v1.5.0
	github.com/lestrrat-go/jwx/v2 v2.0.8
)

require (
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.1.0 // indirect
	github.com/goccy/go-json v0.9.11 // indirect
	github.com/lestrrat-go/blackmagic v1.0.1 // indirect
	github.com/lestrrat-go/httpcc v1.0.1 // indirect
	github.com/lestrrat-go/httprc v1.0.4 // indirect
	github.com/lestrrat-go/iter v1.0.2 // indirect
	github.com/lestrrat-go/option v1.0.0 // indirect
	golang.org/x/crypto v0.5.0 // indirect
)
