package main

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io"
)

func generate() (string, error) {
	rand := rand.Reader
	var buf [32]byte
	if _, err := io.ReadFull(rand, buf[:]); err != nil {
		return "", fmt.Errorf("could not generate PKCE code: %w", err)
	}
	return hex.EncodeToString(buf[:]), nil
}

func challenge(challenge string) string {
	b := sha256.Sum256([]byte(challenge))
	return base64.RawURLEncoding.EncodeToString(b[:])
}
