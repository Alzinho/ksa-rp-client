OpenID Conformance suite KSA RP demo client

this client runs an authorization on OpenID conformance RP test suite.
It does not serve production as it does not open the browser, nor implement a redirect listener.

to build the client use the command below:

 `go mod tidy && go build`

Before starting the client, start the test module!

To start the client, use the command below for mtls

`./ksa-rp-client -alias ksa-rp \
-clientid bc680915-bbd3-45d7-b3c6-2716f4d178ed \
--transportCert="./model_bank/transport.crt" \
--transportKey="./model_bank/transport.key" \
--signingKey="./model_bank/signing.key" \
--encryptionKey="./model_bank/encryption.key" \
--local=true`


To start the client using the private key auth, use the command below:

`./ksa-rp-client -alias ksa-rp \
-clientid bc680915-bbd3-45d7-b3c6-2716f4d178ed \
--transportCert="./model_bank/transport.crt" \
--transportKey="./model_bank/transport.key" \
--signingKey="./model_bank/signing.key" \
--encryptionKey="./model_bank/encryption.key" \
--local=true \
--privateKeyAuth=true`

